const Exceljs = require('exceljs');
const {getDeviceData} = require('./influx.js');
const {calTimestamp,convertTimeZone,lettersToNumber,numberToLetter,
    createRegex,createMeasurementString,divideArray,borderStyle} = require('./utils');

const createDetailsExcel = async (Consumption, devIDs,sensorIDs,startTime,endTime ) =>{
    const workbook= new Exceljs.Workbook();
    createSummaryExcel(Consumption,workbook);
    await createDeviceDataExcel(devIDs,sensorIDs,startTime,endTime,workbook);

    const excel= await workbook.xlsx.writeFile(`Details.xlsx`);

}

const createSummaryExcel = (Consumption,workbook) =>{
        const worksheet= workbook.addWorksheet('Summary');
        const styleObj = borderStyle();
        
        worksheet.columns= [
            {header:'Device',key:'devId',width:20},
            {header:'Sensor',key:'sensorId',width:10},
            {header:'First_Datapoint',key:'startValue',width:16},
            {header:'Last_Datapoint',key:'endValue',width:16},
            {header:'Consumption',key:'consumption',width:15}
        ]

        worksheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
            row.eachCell((cell, colNumber) => {
              if (rowNumber === 1) {
                cell.border = styleObj;
              }
            });
          });
        
        let currRow=2;

        for (const [device, sensor] of Object.entries(Consumption)) {

            const sensorCount=Object.keys(sensor).length;
            
            //merging ,setting value and aligning the merged cell
            worksheet.mergeCells(`A${currRow}:A${currRow + sensorCount - 1}`);
            const mergedCell = worksheet.getCell(`A${currRow}`);
            mergedCell.value = device;
            mergedCell.alignment = {
                vertical: 'middle',
                horizontal: 'center'
              };

            mergedCell.border = styleObj;

            const sen=sensor;
            for (const [sensor, values] of Object.entries(sen)) {
                let cell =  worksheet.getCell(`B${currRow}`);
                cell.value = sensor;
                cell.border = styleObj;

                const val = values;
                let currColindex = 3;
                for(const [values,value] of Object.entries(val)){
                    const currCol = numberToLetter(currColindex);
                    cell= worksheet.getCell(`${currCol}${currRow}`);
                    cell.value = value;
                    cell.border = styleObj;

                    currColindex++;
                }
                currRow++;
              }
          }

        worksheet.getRow(1).eachCell((cell)=>{
            cell.font= {bold:true};
        });

        console.log('Consumption summary has been exported as excel');
}

const createDeviceDataExcel = async (devIDs,sensorIDs,startTime,endTime,workbook) =>{
    try{
        const noOfSensor = sensorIDs.length ;
        const noOfDevice = devIDs.length ;
        //creating regex for all sensors
        const regexString = createRegex(sensorIDs);
        //creating a join string for all devices
        const measurementString = createMeasurementString(devIDs);
        //getting tiimestamp from ISO date time
        const startTimestamp = calTimestamp(startTime);
        const endTimestamp = calTimestamp(endTime);
        //getting all datapoints for all devices and all sensors with one query
        const AllDatapoints = await getDeviceData(measurementString,regexString,startTimestamp,endTimestamp);
        //dividing datapoints to array for each of device
        const dividedDatapoints = divideArray(AllDatapoints,noOfDevice);

        const worksheet= workbook.addWorksheet("Device Data");
        const styleObj = borderStyle();

        //setting start column every time 
        let startColumnIndex = 1;

        for(let i=0;i<noOfDevice;i++){
            //getting the start column name
            const startColumn = numberToLetter(startColumnIndex);
            //Calculating endColumn char 
            const endColumnIndex = startColumnIndex + sensorIDs.length;
            const endColumn = numberToLetter(endColumnIndex);
            //merging ,setting value and aligning the merged cell 
            worksheet.mergeCells(`${startColumn}1:${endColumn}1`);
            const mergedCell = worksheet.getCell(`${startColumn}1`);
            mergedCell.value = devIDs[i];
            mergedCell.alignment = {
                vertical: 'middle',
                horizontal: 'center'
              };

            mergedCell.border = styleObj;
            
            const headings = ['TIME', ...sensorIDs];
            for(let l=0;l<headings.length;l++){
                const currentColumnIndex = startColumnIndex+l;
                const currentColum = numberToLetter(currentColumnIndex);
                let cell = worksheet.getCell(`${currentColum}2`)
                cell.value = headings[l];
                cell.border = styleObj;
            }

            const columnA = worksheet.getColumn(`${startColumn}`);
            columnA.width = 20;
            columnA.numFmt = 'yyyy-mm-dd hh:mm:ss';
            
            const currentDatapoint = dividedDatapoints[i]
            const lengthOfCurrentDeviceDP = currentDatapoint.length;

            let currentRow=3;
            for(let j=0;j<lengthOfCurrentDeviceDP;j+=noOfSensor){
                let cell = worksheet.getCell(`${startColumn}${currentRow}`);
                const convertedTime = convertTimeZone(currentDatapoint[j].time);
                cell.value = convertedTime;
                cell.border = styleObj;

                for(let k=0;k<noOfSensor;k++){
                    const currentColumnIndex = startColumnIndex+k+1;
                    const currentColum = numberToLetter(currentColumnIndex);
                    cell = worksheet.getCell(`${currentColum}${currentRow}`)
                    cell.value = currentDatapoint[k+j].value;
                    cell.border = styleObj;
                }
                currentRow++;
            }

            //setting start column to end column + 2 for next table
            startColumnIndex = endColumnIndex + 2;

            }
            worksheet.getRow(1).eachCell((cell)=>{
                cell.font= {bold:true};
            });
            worksheet.getRow(2).eachCell((cell)=>{
                cell.font= {bold:true};
            });

            console.log('Device data has been exported as excel')
        }
        catch(err){
            console.log(err);
        }
    }


    module.exports={
        createDetailsExcel
}
  