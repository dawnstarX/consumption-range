const {getFirstAndLastValue} = require('./influx');
const {calTimestamp} = require('./utils')

const calculateSingleConsumption = async(startTime,endTime,device,tag) =>{
    const result = await getFirstAndLastValue(startTime,endTime,device,tag);

    let Consumption;

    if (result[0] && 'first_value' in result[0] && 'last_value' in result[0]) {
        const startValue=result[0].first_value;
        const endValue=result[0].last_value;
        //finding consumption for valid time ranges
        const cal=endValue-startValue
         Consumption = {
                startValue,
                endValue,
                consumption:cal
         };
      } else {
        //returning -1 if the data is not available for the time range
        Consumption= {
                startValue:-1,
                endValue:-1,
                consumption:-1
        };
      }
      return Consumption;
}

const findConsumption = async(startTime,endTime,devices,sensors) =>{
  const result = {};
  for (const device of devices) {
    result[device] = {};
  
    for (const sensor of sensors) {
      const startTimestamp = calTimestamp(startTime);
      const endTimestamp = calTimestamp(endTime);
      const value = await calculateSingleConsumption(startTimestamp,endTimestamp,device,sensor);
      result[device][sensor] = value;
    }
  }
  return result;
}


module.exports = findConsumption;









