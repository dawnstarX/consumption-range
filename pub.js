const mqtt = require('mqtt');
require('dotenv').config();
const {getRandomNumber} = require('./utils')

const devID = 'INEM_DEMO';
const topic = `${process.env.MQTT_TOPIC}/${devID}/data`;

const client = mqtt.connect(process.env.MQTT_URL, {
  clientId: 'publisher'
});

const publishData = () => {
    let i = 5;
  
    setInterval(() => {

      const message = {
        device: devID,
        data: [
          {
            tag: 'VOLTS1',
            value: 228.07602856051832 + i,
          },
          {
            tag: 'VOLTS2',
            value: 228.3990800794001 + i,
          },
          {
            tag: 'CUR1',
            value: 1.676028560518326 + i,
          },
          {
            tag: 'CUR2',
            value: 2.776028560518326 + i,
          },
          {
            tag: 'W1',
            value: 0.4260285605183258 + i,
          },
          {
            tag: 'W2',
            value: 0.6460285605183258 + i,
          },
          {
            tag: 'PF1',
            value: 0.8376028560518325 + i,
          },
          {
            tag: 'PF2',
            value: 0.8076028560518327 + i,
          },
          {
            tag: 'FREQ',
            value: 50.076028560518324 + i,
          },
          {
            tag: 'REACTIVE',
            value: 1.306028560518326 + i,
          },
          {
            tag: 'ACTIVE',
            value: 1.326028560518326 + i,
          },
          {
            tag: 'RSSI',
            value: 16.076028560518324 + i,
          },
        ],
      };
      client.publish(topic, JSON.stringify(message));
      console.log('Publishing message');
      const randomNum = getRandomNumber();
      i+=randomNum;
    }, 5000);
  }

publishData();