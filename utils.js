const moment = require('moment');

const getRandomNumber = ()=> {
    const randomNum = Math.floor(Math.random() * 5) + 1;
    return randomNum;
}

const calTimestamp = (time)=>{
    const timestamp = (moment(time).valueOf())*10**6;
    return timestamp;
}

const convertTimeZone = (time) =>{
    const timestamp = moment(time);
    // Adding 5 hours and 30 minutes to the timestamp
    const updatedTimestamp = timestamp.format('YYYY-MM-DD HH:mm:ss');

    return updatedTimestamp;
}

const lettersToNumber = (letters) =>{
  const number = letters.split('').reduce((r, a) => r * 26 + parseInt(a, 36) - 9, 0);
  return number;
}

const  numberToLetter = (number) => {
  let result = '';
  while (number > 0) {
    const remainder = (number - 1) % 26;
    result = String.fromCharCode(65 + remainder) + result;
    number = Math.floor((number - 1) / 26);
  }
  return result;
}

  

const createRegex = (sensorIDs) =>{
    // Create regex pattern from array values
    const regexPattern = new RegExp(`${sensorIDs.join('|')}`);

    return regexPattern;
}

const createMeasurementString = (devIDs) =>{
    const measurementString = devIDs.join(',');
    return measurementString;
}

const divideArray = (array, n) => {
    const chunkSize = array.length / n;
    const result = [];
  
    for (let i = 0; i < array.length; i += chunkSize) {
      const chunk = array.slice(i, i + chunkSize);
      result.push(chunk);
    }
  
    return result;
}

const borderStyle = () =>{
  const borderThickness = 'medium'
  const styleObj = {
    top:  {style: `${borderThickness}`},
    left: {style: `${borderThickness}`},
    bottom:{style:`${borderThickness}`},
    right:{style: `${borderThickness}`}
  }

  return styleObj;
}

module.exports={getRandomNumber,calTimestamp,convertTimeZone,lettersToNumber,
  numberToLetter,createRegex,createMeasurementString,divideArray,borderStyle}

