const express = require('express');
const path = require('path');
require('dotenv').config();
const findConsumption = require('./consumption')
const validate =  require('./validate');
const {createDetailsExcel} = require('./exceljs');
const {validateDevIds,validateSensorIds,validateStartTime,validateEndtime} = require('./validations')


const app = express();

app.use(express.json());


app.post('/consumption',[validateDevIds,validateSensorIds,validateStartTime,validateEndtime],validate, async(req, res) => {
    const { devIds, sensorIds, startTime, endTime } = req.body;

    const result = await findConsumption(startTime,endTime,devIds,sensorIds);

    await createDetailsExcel(result,devIds,sensorIds,startTime,endTime);

    const fileName = 'Details.xlsx'; 
    const filePath = path.join(__dirname, fileName);

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename="${fileName}"`);

    res.sendFile(filePath, (err) => {
      if (err) {
        console.error('Error sending file:', err);
      } else {
        console.log('File sent successfully');
      }
    });
  });


app.listen(process.env.API_PORT, () => {
    console.log(`Server is running on port ${process.env.API_PORT}`);
  });