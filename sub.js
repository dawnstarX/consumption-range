const mqtt = require('mqtt');
require('dotenv').config();
const {insertData} = require('./influx');

const devID = 'INEM_DEMO';
const topic = `${process.env.MQTT_TOPIC}/${devID}/data`;

const client = mqtt.connect(process.env.MQTT_URL, {
  clientId: 'subscriber',
});

client.on('connect', () => {
    console.log("connected to broker")
  client.subscribe(topic, (err) => {
    if (err) {
      console.log(err);
      return;
    }

    client.on('message', async(topic, payload) => {
        console.log(`Received message`);
        const { data} = JSON.parse(payload);
        await insertData('INEM_DEMO1',data);
        await insertData('INEM_DEMO2',data);
        await insertData('INEM_DEMO3',data);
        console.log("inserted into influxDB");
    });
  });
});

client.on('error', (err) => {
  console.log(err);
});

