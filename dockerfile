# Use a base Node.js image
FROM node:18

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Set ENV variable for influxs
ENV TZ=Asia/Kolkata
ENV DB_HOST=127.0.0.1


# 
EXPOSE 3000

# Set the command to run when the container starts
CMD ["node", "app.js"]
