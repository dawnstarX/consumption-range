const { body } = require('express-validator');


const validateDevIds=body('devIds').isArray({ min: 1, max: 10 }).withMessage('DevIds should be an array with min 1 element and max 10 elements');
const validateSensorIds=body('sensorIds').isArray({ min: 1, max: 15 }).withMessage('sensorIds should be an array with min 1 element and max 15 elements');;
const validateStartTime=body('startTime').isISO8601().withMessage('startTime should be in ISO 8601 format i.e. YYYY-MM-DD HH:mm:ss')
const validateEndtime=body('endTime').isISO8601().withMessage('startTime should be in ISO 8601 format i.e. YYYY-MM-DD HH:mm:ss')


module.exports={
    validateDevIds,
    validateSensorIds,
    validateStartTime,
    validateEndtime
}