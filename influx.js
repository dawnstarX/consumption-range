const Influx=require('influx');
require('dotenv').config();


//influxDB config
const influx = new Influx.InfluxDB({
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,  
    port: process.env.DB_PORT,        
})

//function to insert data to device measurement
const insertData = async (device, data) => {
    const points = data.map(({ tag, value }) => ({
      measurement: device,
      tags: { sensor:tag },
      fields: { value: value.toFixed(2) },
    }));

    try {
      await influx.writePoints(points);
      console.log('successfully inserted data into Influx DB');
    } catch (error) {
      console.error(error);
    }
  }


//function to get the first and the last value within given timestamp
const getFirstAndLastValue = async(startTimestamp,endTimestamp,device,tag)=> {
    try{
        const result= await influx.query(`SELECT FIRST("value") AS first_value, LAST("value") AS last_value 
        FROM "${device}"
        WHERE "sensor"='${tag}' AND
        time >= ${startTimestamp} AND 
        time <= ${endTimestamp}`)

        return result;
      }
      catch(err){
        console.log(err);
      }
    }
    
    const getDeviceData =async(measurementString,regexString,startTimestamp,endTimestamp) =>{
      try{
        const queryStr = `SELECT * FROM ${measurementString} WHERE "sensor"=~ ${regexString} AND time >= ${startTimestamp} AND 
        time <= ${endTimestamp}`
        const result= await influx.query(queryStr);
        return result;
  }
  catch(err){
    console.log(err);
  }
}



module.exports= {insertData,getFirstAndLastValue,getDeviceData};